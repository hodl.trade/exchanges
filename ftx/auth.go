package ftx

import (
	"bytes"
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"hash"
	"net/http"
	"strconv"
	"sync"
	"time"

	"hodl.trade/exchanges"
)

type Credentials struct {
	ApiKey     string
	ApiSecret  string
	Subaccount string

	lock   sync.Mutex
	hmac   hash.Hash
	secret string
}

type ctxKey int

const (
	ctxKeyCredentials ctxKey = iota
)

func (c *Credentials) Wrap(ctx context.Context) context.Context {
	return context.WithValue(ctx, ctxKeyCredentials, c)
}

func sign(ctx context.Context) (c *Credentials) {
	c, _ = ctx.Value(ctxKeyCredentials).(*Credentials)
	return
}

func (c *Credentials) Apply(req *http.Request) error {
	if c == nil {
		return exchanges.ErrCredentialsRequired
	}

	ts := strconv.FormatInt(time.Now().Unix()*1000, 10)

	b := bytes.NewBuffer(nil)
	b.WriteString(ts)
	b.WriteString(req.Method)
	b.WriteString(req.URL.EscapedPath())
	if req.URL.RawQuery != "" {
		b.WriteString("?")
		b.WriteString(req.URL.RawQuery)
	}
	if req.Body != http.NoBody && req.Body != nil {
		r, _ := req.GetBody()
		b.ReadFrom(r)
	}

	c.lock.Lock()
	if c.hmac == nil || c.secret != c.ApiSecret {
		c.hmac = hmac.New(sha256.New, []byte(c.ApiSecret))
		c.secret = c.ApiSecret
	} else {
		c.hmac.Reset()
	}
	c.hmac.Write(b.Bytes())
	sum := c.hmac.Sum(nil)
	c.lock.Unlock()

	req.Header.Set("FTX-KEY", c.ApiKey)
	req.Header.Set("FTX-SIGN", hex.EncodeToString(sum))
	req.Header.Set("FTX-TS", ts)
	if c.Subaccount != "" {
		req.Header.Set("FTX-SUBACCOUNT", c.Subaccount)
	}

	return nil
}
