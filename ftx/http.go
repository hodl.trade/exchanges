package ftx

import (
	"context"
	"errors"
	"io"
	"net/http"
	"net/url"

	"hodl.trade/exchanges/httputil"
)

var (
	API_URL = httputil.BaseURL("https://ftx.com/api")
)

const (
	ErrRateLimit = ResponseError("Too many requests")
)

type ResponseError string

func (t ResponseError) Error() string {
	return string(t)
}

// Shortcuts

func Get(ctx context.Context, path string, query url.Values, payload interface{}) (res *http.Response, err error) {
	return DefaultHttpClient.http.Get(ctx, API_URL(path), query, payload)
}

func Post(ctx context.Context, path string, body interface{}, payload interface{}) (res *http.Response, err error) {
	return DefaultHttpClient.http.Post(ctx, API_URL(path), nil, body, payload)
}

var DefaultHttpClient = NewHttpClient(nil)

type Client struct {
	http *httputil.Client
}

func NewHttpClient(client *http.Client) *Client {
	if client == nil {
		client = http.DefaultClient
	}
	return &Client{httputil.NewClient(client, &HttpAdapter{})}
}

type HttpAdapter struct {
	json *httputil.JSON
}

type Response struct {
	Error   ResponseError `json:"error,omitempty"`
	Result  interface{}   `json:"result,omitempty"`
	Success bool          `json:"success"`
}

var ErrNoBody = errors.New("no body")

func (a *HttpAdapter) Encode(payload interface{}) (io.Reader, error) {
	return a.json.Encode(payload)
}

func (a *HttpAdapter) Apply(req *http.Request) (err error) {
	return a.json.Apply(req)
}

func (a *HttpAdapter) Process(res *http.Response, result interface{}) (err error) {
	r := &Response{Result: result}

	if err = a.json.Process(res, r); err != nil {
		return
	}

	// TODO check for 400 and unauthorized etc...

	if !r.Success {
		return r.Error
	}

	return nil
}
