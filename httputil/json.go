package httputil

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
)

type JSON struct{}

func (*JSON) Encode(payload interface{}) (io.Reader, error) {
	if payload == nil {
		return nil, nil
	}

	b := bytes.NewBuffer(nil)
	if err := json.NewEncoder(b).Encode(payload); err != nil {
		return nil, err
	}

	return b, nil
}

func (*JSON) Apply(req *http.Request) error {
	req.Header.Set("Accept", "application/json")
	return nil
}

func (*JSON) Process(res *http.Response, result interface{}) (err error) {
	if res.Body == http.NoBody {
		// TODO respect redirects and head/options method
		return ErrNoBody
	}

	if err = json.NewDecoder(res.Body).Decode(result); err != nil {
		return
	}

	return nil
}
