package httputil

import (
	"net/url"
	"strings"
)

func BaseURL(url string) func(path ...string) string {
	url = strings.TrimRight(url, "/")

	return func(path ...string) string {
		switch len(path) {
		case 0:
			return url
		case 1:
			return url + "/" + path[0]
		default:
			return url + "/" + strings.Join(path, "/")
		}
	}
}

func appendQuery(url string, query url.Values) string {
	if q := query.Encode(); q != "" {
		return url + "?" + q
	}

	return url
}
