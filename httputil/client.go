package httputil

import (
	"context"
	"errors"
	"io"
	"net/http"
	"net/url"
)

var (
	ErrNoBody = errors.New("no body")
)

type Adapter interface {
	Modifier
	Encode(payload interface{}) (io.Reader, error)
	Process(res *http.Response, result interface{}) (err error)
}

type Client struct {
	c *http.Client
	a Adapter
}

type Modifier interface {
	Apply(*http.Request) error
}

func NewClient(client *http.Client, adapter Adapter) (c *Client) {
	if client == nil {
		client = http.DefaultClient
	}
	return &Client{c: client, a: adapter}
}

func (c *Client) Get(ctx context.Context, url string, query url.Values, result interface{}, modifiers ...Modifier) (res *http.Response, err error) {
	return c.Do(ctx, http.MethodGet, url, query, nil, result, modifiers...)
}

func (c *Client) Post(ctx context.Context, url string, query url.Values, body interface{}, result interface{}, modifiers ...Modifier) (res *http.Response, err error) {
	return c.Do(ctx, http.MethodPost, url, query, body, result, modifiers...)
}

func (c *Client) Do(ctx context.Context, method, url string, query url.Values, payload interface{}, result interface{}, modifiers ...Modifier) (res *http.Response, err error) {
	var body io.Reader
	if body, err = c.a.Encode(payload); err != nil {
		return nil, err
	}

	if q := query.Encode(); q != "" {
		url += "?" + q
	}

	var req *http.Request
	if req, err = http.NewRequestWithContext(ctx, method, url, body); err != nil {
		return nil, err
	}

	if err = c.a.Apply(req); err != nil {
		return nil, err
	}

	for _, m := range modifiers {
		if err = m.Apply(req); err != nil {
			return nil, err
		}
	}

	res, err = c.c.Do(req)
	if res != nil && res.Body != nil {
		defer res.Body.Close()
	}
	if err != nil {
		return
	}

	return res, c.a.Process(res, result)
}

// Shortcuts

type ModifierFn func(req *http.Request) error

func (fn ModifierFn) Apply(req *http.Request) error {
	return fn(req)
}

func Header(key, value string) Modifier {
	return ModifierFn(func(req *http.Request) error {
		req.Header.Set(key, value)
		return nil
	})
}
