package binance

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"

	"hodl.trade/exchanges/httputil"
)

var (
	API_V1_URL         = httputil.BaseURL("https://api.binance.com/api/v1")
	API_V3_URL         = httputil.BaseURL("https://api.binance.com/api/v3")
	API_FUTURES_V1_URL = httputil.BaseURL("https://fapi.binance.com/api/v1")

	SAPI_V1_URL = httputil.BaseURL("https://api.binance.com/sapi/v1")
)

type ResponseError struct {
	Code    int    `json:"code"`
	Message string `json:"msg"`
}

func (t *ResponseError) Error() string {
	return fmt.Sprintf("%d: %s", t.Code, t.Message)
}

var DefaultHttpClient = NewHttpClient(nil)

type Client struct {
	http *httputil.Client
}

func NewHttpClient(client *http.Client) *Client {
	if client == nil {
		client = http.DefaultClient
	}
	return &Client{httputil.NewClient(client, &HttpAdapter{})}
}

type HttpAdapter struct {
	json *httputil.JSON
}

func (a *HttpAdapter) Encode(payload interface{}) (io.Reader, error) {
	if payload == nil {
		return nil, nil
	}

	q, ok := payload.(url.Values)
	if !ok {
		return nil, errors.New("payload must be nil or instance of url.Values")
	}

	return strings.NewReader(q.Encode()), nil
}

func (a *HttpAdapter) Apply(req *http.Request) (err error) {
	if req.Body != nil {
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	}
	if err = a.json.Apply(req); err != nil {
		return
	}

	return
}

func (a *HttpAdapter) Process(res *http.Response, result interface{}) (err error) {
	if res.StatusCode < 200 || 300 <= res.StatusCode {
		var e *ResponseError
		if err = a.json.Process(res, &e); err != nil {
			return err
		}
		return e
	} else if res.StatusCode == http.StatusNoContent {
		return nil
	}

	if err = a.json.Process(res, result); err != nil {
		return
	}

	return nil
}
