package binance

import (
	"bytes"
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"hash"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"sync"
	"time"

	"hodl.trade/exchanges"
)

type Credentials struct {
	ApiKey    string
	SecretKey string

	lock   sync.Mutex
	hmac   hash.Hash
	secret string
}

type ctxKey int

const (
	ctxKeyCredentials ctxKey = iota
)

func (c *Credentials) Wrap(ctx context.Context) context.Context {
	return context.WithValue(ctx, ctxKeyCredentials, c)
}

func sign(ctx context.Context) *Credentials {
	c, _ := ctx.Value(ctxKeyCredentials).(*Credentials)
	return c
}

func (c *Credentials) Apply(req *http.Request) error {
	if c == nil {
		return exchanges.ErrCredentialsRequired
	}

	ts := strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)

	var body *bytes.Buffer
	if req.Body != http.NoBody && req.Body != nil {
		body = bytes.NewBuffer(nil)
		n, _ := io.Copy(body, req.Body)
		if n == 0 {
			body = nil
		}
		req.Body.Close()
	}

	if body == nil {
		if req.URL.RawQuery != "" {
			req.URL.RawQuery += "&"
		}
		req.URL.RawQuery += "timestamp=" + url.QueryEscape(ts)
	} else {
		body.WriteString("&timestamp=" + url.QueryEscape(ts))
	}

	c.lock.Lock()
	if c.hmac == nil || c.secret != c.SecretKey {
		c.hmac = hmac.New(sha256.New, []byte(c.SecretKey))
		c.secret = c.SecretKey
	} else {
		c.hmac.Reset()
	}
	if req.URL.RawQuery != "" {
		c.hmac.Write([]byte(req.URL.RawQuery))
	}
	if body != nil {
		c.hmac.Write(body.Bytes())
	}
	sum := c.hmac.Sum(nil)
	c.lock.Unlock()

	req.Header.Set("X-MBX-APIKEY", c.ApiKey)

	s := "signature=" + url.QueryEscape(hex.EncodeToString(sum))
	if body == nil && req.Method == http.MethodGet {
		req.URL.RawQuery += "&" + s
	} else {
		if body == nil {
			body = bytes.NewBuffer([]byte("&"))
		}
		body.WriteString(s)
	}

	if body != nil {
		req.Body = io.NopCloser(body)
		req.ContentLength = int64(body.Len())
		buf := body.Bytes()
		req.GetBody = func() (io.ReadCloser, error) {
			r := bytes.NewReader(buf)
			return io.NopCloser(r), nil
		}
	} else if req.Body != nil && req.Body != http.NoBody {
		req.Body = nil
		req.GetBody = nil
	}

	return nil
}
