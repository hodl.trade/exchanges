package gate

import (
	"bytes"
	"context"
	"crypto/hmac"
	"crypto/sha512"
	"encoding/hex"
	"hash"
	"io"
	"net/http"
	"strconv"
	"sync"
	"time"

	"hodl.trade/exchanges"
)

type Credentials struct {
	ApiKey    string
	ApiSecret string

	lock   sync.Mutex
	hmac   hash.Hash
	secret string
}

type ctxKey int

const (
	ctxKeyCredentials ctxKey = iota
)

func (c *Credentials) Wrap(ctx context.Context) context.Context {
	return context.WithValue(ctx, ctxKeyCredentials, c)
}

func sign(ctx context.Context) *Credentials {
	c, _ := ctx.Value(ctxKeyCredentials).(*Credentials)
	return c
}

// docs: https://www.gate.io/docs/apiv4/en/index.html#api-signature-string-generation
func (c *Credentials) Apply(req *http.Request) error {
	if c == nil {
		return exchanges.ErrCredentialsRequired
	}

	b := bytes.NewBuffer(nil)
	// method
	b.WriteString(req.Method)
	b.WriteString("\n")
	// path
	b.WriteString(req.URL.EscapedPath())
	b.WriteString("\n")
	// query
	if req.URL.RawQuery != "" {
		b.WriteString(req.URL.RawQuery)
	}
	b.WriteString("\n")
	// payload
	{
		h := sha512.New()
		if req.Body != nil && req.Body != http.NoBody {
			p, _ := req.GetBody()
			io.Copy(h, p)
			p.Close()
		}
		b.WriteString(hex.EncodeToString(h.Sum(nil)))
		b.WriteString("\n")
	}
	// timestamp
	ts := strconv.FormatInt(time.Now().Unix(), 10)
	b.WriteString(ts)

	c.lock.Lock()
	if c.hmac == nil || c.secret != c.ApiSecret {
		c.hmac = hmac.New(sha512.New, []byte(c.ApiSecret))
		c.secret = c.ApiSecret
	} else {
		c.hmac.Reset()
	}
	c.hmac.Write(b.Bytes())
	sum := c.hmac.Sum(nil)
	c.lock.Unlock()

	req.Header.Set("KEY", c.ApiKey)
	req.Header.Set("SIGN", hex.EncodeToString(sum))
	req.Header.Set("Timestamp", ts)

	return nil
}
