package gate

import (
	"context"
	"io"
	"net/http"
	"net/url"

	"hodl.trade/exchanges/httputil"
)

var API_URL = httputil.BaseURL("https://api.gateio.ws/api/v4")

type ErrorResponse struct {
	Label   string `json:"label"`
	Message string `json:"message"`
}

func (t ErrorResponse) Error() string {
	return t.Label + ": " + t.Message
}

// Shortcuts

func Get(ctx context.Context, path string, query url.Values, payload interface{}) (res *http.Response, err error) {
	return DefaultHttpClient.http.Get(ctx, API_URL(path), query, payload)
}

func Post(ctx context.Context, path string, body interface{}, payload interface{}) (res *http.Response, err error) {
	return DefaultHttpClient.http.Post(ctx, API_URL(path), nil, body, payload)
}

var DefaultHttpClient = NewHttpClient(nil)

type Client struct {
	http *httputil.Client
}

func NewHttpClient(client *http.Client) *Client {
	return &Client{httputil.NewClient(client, &HttpAdapter{})}
}

type HttpAdapter struct {
	json *httputil.JSON
}

func (a *HttpAdapter) Encode(payload interface{}) (io.Reader, error) {
	return a.json.Encode(payload)
}

func (a *HttpAdapter) Apply(req *http.Request) (err error) {
	return a.json.Apply(req)
}

func (a *HttpAdapter) Process(res *http.Response, result interface{}) (err error) {
	if res.StatusCode == http.StatusNoContent {
		return nil
	}

	if 200 <= res.StatusCode && res.StatusCode < 300 {
		err = a.json.Process(res, result)
		return
	}

	var errres *ErrorResponse
	if err = a.json.Process(res, &errres); err != nil {
		return
	}
	err = errres
	return
}
