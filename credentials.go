package exchanges

import (
	"errors"
)

var (
	ErrCredentialsRequired = errors.New("credentials required")
)
